#include <gtest/gtest.h>
#include "polyfit.hpp"

TEST(PolyfitTest, OnlyUniqueInputValues) {
    EXPECT_TRUE(polyfit_check_input_rank({0.0, 1.0, 2.0, 3.0}, 3));
    EXPECT_FALSE(polyfit_check_input_rank({0.0, 1.0, 2.0, 3.0}, 4));
    EXPECT_FALSE(polyfit_check_input_rank({0.0, 1.0, 2.0}, 3));
    EXPECT_TRUE(polyfit_check_input_rank({0.0, 1.0, 2.0, 3.0, 4.0}, 3));
    EXPECT_FALSE(polyfit_check_input_rank({}, 0));
    EXPECT_TRUE(polyfit_check_input_rank({0.0}, 0));
}

TEST(PolyfitTest, NonUniqueInputValuesSorted) {
    EXPECT_FALSE(polyfit_check_input_rank({1.0, 1.0, 2.0, 3.0}, 3));
    EXPECT_TRUE(polyfit_check_input_rank({0.0, 1.0, 1.0, 2.0, 3.0, 3.0}, 3));
    EXPECT_FALSE(polyfit_check_input_rank({0.0, 1.0, 1.0, 2.0, 3.0, 3.0}, 4));
    EXPECT_TRUE(polyfit_check_input_rank({0.0, 0.0, 0.0, 0.0}, 0));
    EXPECT_FALSE(polyfit_check_input_rank({0.0, 0.0, 0.0, 0.0}, 1));
}

TEST(PolyfitTest, NonUniqueInputValuesUnsorted) {
    EXPECT_FALSE(polyfit_check_input_rank({3.0, 1.0, 2.0, 1.0}, 3));
    EXPECT_TRUE(polyfit_check_input_rank({2.0, 1.0, 3.0, 0.0, 1.0, 3.0}, 3));
    EXPECT_FALSE(polyfit_check_input_rank({2.0, 1.0, 3.0, 0.0, 1.0, 3.0}, 4));
}
