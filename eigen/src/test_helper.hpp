#ifndef TEST_HELPER_HPP
#define TEST_HELPER_HPP
#include <vector>
bool are_double_vectors_almost_equal(const std::vector<double>& v1, const std::vector<double>& v2, double tolerance);
#endif //TEST_HELPER_HPP
