#include "test_helper.hpp"

#include "Eigen/Dense"
bool are_double_vectors_almost_equal(const std::vector<double>& v1, const std::vector<double>& v2, double tolerance) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        if (std::abs(v1[i] - v2[i]) > tolerance) {
            return false;
        }
    }
    return true;
}