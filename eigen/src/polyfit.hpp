#ifndef UEBUNG7_EIGEN_POLYFIT_H
#define UEBUNG7_EIGEN_POLYFIT_H

#include <cstddef>
#include <vector>

std::vector<double> polyfit3(const std::vector<double>& x, const std::vector<double>& y);

std::vector<double> polyfit(const std::vector<double>& x, const std::vector<double>& y,
                            size_t order);

bool polyfit_check_input_rank(const std::vector<double>& x, size_t order);

#endif  // UEBUNG7_EIGEN_POLYFIT_H
