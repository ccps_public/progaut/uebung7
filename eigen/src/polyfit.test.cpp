#include "polyfit.hpp"
#include <cmath>
#include <gtest/gtest.h>
#include <test_helper.hpp>

TEST(PolyfitTest, ExactSolutionOrder3Constant) {
    std::vector<double> x{0.0, 1.0, 2.0, 3.0};
    std::vector<double> y1{-1.0, -1.0, -1.0, -1.0};

    std::vector<double> p = polyfit(x, y1, 3);
    std::vector<double> p_soll{0.0, 0.0, 0.0, -1.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, ExactSolutionOrder0OverdeterminedConstant) {
    std::vector<double> x{0.0, 1.0, 2.0, 3.0};
    std::vector<double> y1{-1.0, -1.0, -1.0, -1.0};

    std::vector<double> p = polyfit(x, y1, 0);
    std::vector<double> p_soll{-1.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, ExactSolutionOrder0Constant) {
    std::vector<double> x{10.0};
    std::vector<double> y1{42.0};

    std::vector<double> p = polyfit(x, y1, 0);
    std::vector<double> p_soll{42.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, WithNoiseOrder0Constant) {
    std::vector<double> x{10.0, 20.0, -2.0, 33.0};
    std::vector<double> y1{5.5, 6.7, 4.5, 3.3};

    std::vector<double> p = polyfit(x, y1, 0);
    std::vector<double> p_soll{5.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, ApproxOrder1Constant) {
    std::vector<double> x{10.0, 25.0, 38.0, 40.0};
    std::vector<double> y1{22, 24, 24, 22};

    std::vector<double> p = polyfit(x, y1, 0);
    std::vector<double> p_soll{23.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}
