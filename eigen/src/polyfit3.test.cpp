#include <gtest/gtest.h>
#include "polyfit.hpp"
#include <cmath>

#include "test_helper.hpp"

TEST(PolyfitTest, ExactSolutionConstant) {
    std::vector<double> x{0.0, 1.0, 2.0, 3.0};
    std::vector<double> y1{-1.0, -1.0, -1.0, -1.0};

    std::vector<double> p = polyfit3(x, y1);
    std::vector<double> p_soll{0.0, 0.0, 0.0, -1.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, ExactSolutionOverdeterminedConstant) {
    std::vector<double> x{0.0, 1.0, 2.0, 3.0, 4.0, 5.0};
    std::vector<double> y1{-1.0, -1.0, -1.0, -1.0, -1.0, -1.0};

    std::vector<double> p = polyfit3(x, y1);
    std::vector<double> p_soll{0.0, 0.0, 0.0, -1.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, ExactSolutionLinear) {
    std::vector<double> x{0.0, 1.0, 2.0, 3.0};
    std::vector<double> y1{-10.0, -5.0, 0.0, 5.0};

    std::vector<double> p = polyfit3(x, y1);
    std::vector<double> p_soll{0.0, 0.0, 5.0, -10.0};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, ExactSolutionCubic) {
    std::vector<double> x{-1.0, 1.0, 2.0, 3.0};
    std::vector<double> y1{-0.8, 1.2, 8.2, 27.2};

    std::vector<double> p = polyfit3(x, y1);
    std::vector<double> p_soll{1.0, 0.0, 0.0, 0.2};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}

TEST(PolyfitTest, WithNoiseCubic) {
    std::vector<double> x{-1.0, 1.0, 2.0, 3.0, 0.0, 0.0, 0.0, 0.0};
    std::vector<double> y1{-0.8, 1.2, 8.2, 27.2, 0.3, 0.1, 0.3, 0.1};

    std::vector<double> p = polyfit3(x, y1);
    std::vector<double> p_soll{1.0, 0.0, 0.0, 0.2};

    EXPECT_TRUE(are_double_vectors_almost_equal(p, p_soll, 1e-6));
}
