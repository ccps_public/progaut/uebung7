#include <algorithm>
#include <gtest/gtest.h>
#include <opencv2/opencv.hpp>

#include "opencv_exercise.hpp"

void compare_images(const cv::Mat& input, const cv::Mat& correct) {
    ASSERT_EQ(input.rows, correct.rows);
    ASSERT_EQ(input.cols, correct.cols);
    ASSERT_EQ(input.channels(), correct.channels());
    ASSERT_EQ(input.depth(), correct.depth());

    ASSERT_TRUE(std::equal(input.begin<uint8_t>(), input.end<uint8_t>(), correct.begin<uint8_t>()));
}

TEST(OpenCVExerciseTest, FindHotPixel) {
    const cv::Mat img = cv::imread("../input/rover.jpg");
    const Point p = findHotPixel(img);
    ASSERT_EQ(p.x, 1284);
    ASSERT_EQ(p.y, 792);
}

TEST(OpenCVExerciseTest, Grayscale) {
    const cv::Mat img = cv::imread("../input/rover.jpg");
    const cv::Mat original = img.clone();
    const cv::Mat img_correct = cv::imread("../output/grayscale_correct.png");
    const cv::Mat result = grayscale(img);
    cv::imwrite("../output/grayscale.png", result);
    compare_images(result, img_correct);
    compare_images(img, original);
}
