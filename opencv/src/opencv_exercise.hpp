#ifndef UEBUNG7_OPENCV_DEMO_H
#define UEBUNG7_OPENCV_DEMO_H
#include <opencv2/opencv.hpp>

struct Point {
    int x;
    int y;
};

Point findHotPixel(const cv::Mat& image);
cv::Mat grayscale(const cv::Mat& input);
cv::Mat gaussianBlur(const cv::Mat& input);
cv::Mat borderDetect(const cv::Mat& input);

#endif  // UEBUNG7_OPENCV_DEMO_H
