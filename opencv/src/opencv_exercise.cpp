#include "opencv_exercise.hpp"

#include <iostream>

Point findHotPixel(const cv::Mat& image) {
    return {0,0};
}

cv::Mat grayscale(const cv::Mat& input) {
    return input;
}

cv::Mat gaussianBlur(const cv::Mat& image) {
    return image;
}

cv::Mat borderDetect(const cv::Mat& image) {
    return image;
}
