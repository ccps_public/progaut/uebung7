#include <iostream>
#include <opencv2/opencv.hpp>

int main() {
    const cv::Mat img = cv::imread("../input/rover.jpg");

    cv::imshow("img", img);
    cv::waitKey(0);

    return 0;
}
