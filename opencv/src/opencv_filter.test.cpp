#include <algorithm>
#include <gtest/gtest.h>
#include <opencv2/opencv.hpp>

#include "opencv_exercise.hpp"

void compare_images(const cv::Mat& input, const cv::Mat& correct) {
    ASSERT_EQ(input.rows, correct.rows);
    ASSERT_EQ(input.cols, correct.cols);
    ASSERT_EQ(input.channels(), correct.channels());
    ASSERT_EQ(input.depth(), correct.depth());

    ASSERT_TRUE(std::equal(input.begin<uint8_t>(), input.end<uint8_t>(), correct.begin<uint8_t>()));
}

TEST(OpenCVExerciseTest, Blur) {
    const cv::Mat img = cv::imread("../input/rover.jpg");
    const cv::Mat img_correct = cv::imread("../output/blur_correct.png");
    const cv::Mat result = gaussianBlur(img);
    cv::imwrite("../output/blur.png", result);
    compare_images(result, img_correct);
}

TEST(OpenCVExerciseTest, BorderDetection) {
    const cv::Mat img = cv::imread("../input/rover.jpg");
    const cv::Mat img_correct = cv::imread("../output/border_correct.bmp");
    const cv::Mat result = borderDetect(img);
    cv::imwrite("../output/border.bmp", result);
    compare_images(result, img_correct);
}
